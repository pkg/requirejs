# Installation
> `npm install --save @types/requirejs`

# Summary
This package contains type definitions for requirejs (http://requirejs.org/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/requirejs.

### Additional Details
 * Last updated: Tue, 07 Nov 2023 15:11:36 GMT
 * Dependencies: none

# Credits
These definitions were written by [Josh Baldwin](https://github.com/jbaldwin).
